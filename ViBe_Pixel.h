#ifndef __VIBE_PIXEL_H__
#define __VIBE_PIXEL_H__

/*
 * Class to represent a single pixel within the ViBe background model
 * Stores a set of values that capture the historical value at the pixel location, and
 * allows for new pixels to be compared to this list to determine if they are foreground
 * or background. The list of pixels can also be updated, to allow new information on
 * the background to be incorporated.
 *
 */

class ViBe_Pixel
{
public:

    //default constructor
    ViBe_Pixel();

    //update the background to first frame
   void backgroundUpdate(int red, int blue, int green);

    //compare the background to the next frame
	void backgroundCompare( int red,  int blue,  int green, int Radius, int& count, int& index);	//compares new pixel to background

protected:

    //RGB values will be stored here
    int RGBPixel[3];

private:

};

#endif
