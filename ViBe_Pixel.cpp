#include "ViBe_Pixel.h"
#include <math.h>
#include <vil/vil_image_view.h>
#include <vbl/vbl_array_2d.h>
#include <vnl/vnl_random.h>


ViBe_Pixel::ViBe_Pixel(){}

void ViBe_Pixel::backgroundUpdate(int red, int blue, int green){

        RGBPixel[0] = red;      //assign the new red value to the RGB[0]
        RGBPixel[1] = blue;     //assign the new blur value to the RGB[1]
        RGBPixel[2] = green;    //assign the new green value to the RGB[2]

}

void ViBe_Pixel::backgroundCompare( int red,  int blue,  int green, int Radius, int& count, int& index){	//compares new pixel to background input could be the pixel colour

    int distance = 0, inputRGB[3] = {red, blue, green}; //create variables and store the input colours in an array making the loop easier to iterate

    for (int i = 0 ; i < 3 ; i++){

        distance += pow(RGBPixel[i] - inputRGB[i],2);     //compare red to red blue to blue green to green and grab there distance

    }

    distance = sqrt(distance);      // finish off the euclidean distance formula

    if (distance < Radius){

        count++;
    }

    index++;

}
