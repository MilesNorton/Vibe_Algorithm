#include "ViBe_Model.h"


#include <vil/vil_image_view.h>
#include <vil/vil_load.h>
#include <vil/vil_save.h>
#include <vul/vul_file_iterator.h>
#include <vul/vul_file.h>
#include <vcl_vector.h>
#include <vul/vul_arg.h>

//Self implemented libraries
#include <vil/algo/vil_gauss_filter.h>  // 2d filter for better accuracy
#include <vcl_iostream.h>
#include <iostream>
#include <sstream>
#include <vcl_iostream.h>
#include <vcl_sstream.h>
#include <iomanip>      // allows set precision member for double types
#include <fstream>      //creates output file .txt
using namespace std;  // needed for output txt file (ofstream)

/*
 * Function that will handle the input arguments by the user.
 * It will then return all the arguments back to the user
*/
void argumentHandler(int argc, char * argv[], vcl_vector<vcl_string> &filenames, vcl_string &directory,
                     vcl_string &extension, vcl_string &outputDirectory, int  &inputRadius,  int &inputBackgroundSamples,
                     int &inputSubSampling, int &inputSamples, vcl_string &computeAccuracyAlgorithm, vcl_string &outputFileName);

/*
 * Function that will handle the vibe algorithm segmentation of each image specified by the user
 * It will return an image counter which represents the amount of images it has segmented
 * before it starts the algorithm it will apply a blur on the image allowing for a better output
*/
void vibeAlgorithm(vcl_vector<vcl_string>filenames,vcl_string outputDirectory, vcl_string extension,vcl_string outputFileName,
                   int &width,int &height,int inputSamples,int inputRadius, int inputBackgroundSamples,
                   int inputSubSampling,int &imageCount);

/*
 * Function that will handle the accuracy algorithm.
 * It compares the last image taken from the vibe algoirthm to the ground truth
 * of the the image
 * It will then save a txt file of the true and false values at 3 decimal places
*/
void accuracyAlgorithm(vcl_string outputDirectory, vcl_string Directory, vcl_string extension,vcl_string outputFileName,
                       int imageCount,int truePositive, int falsPositive,  int trueNegative, int falseNegative, int width,
                       int height, double &tn_rate, double &fn_rate, double &tp_rate, double &fp_rate);

/*
 * Function that will save output images
 * It wills ave the image to the specified output directory with the extension supplied
 * It will be named 'output' followed by an integer specified by the user or program
*/
void saveOutputImage(vcl_string outputDirectory , vcl_string extension, vcl_string outputFileName, int outputNumber,
                     vil_image_view<unsigned char> imageToSave);

/*
 * Function that finds the groundtruth from the specified directory and outputs it to the
 * output directory as the specified extension.
 * It will also return the new groudn truth as an image to be used
*/
vil_image_view<unsigned char>  grabGroundTruth( vcl_string directory, vcl_string outputDirectory, vcl_string extension);


/*
 * Main program to run the ViBe motion detection algorithm.
 * This program will
 *  - take a list of images on the command line, specified as a path and a partial filename (i.e. *.png to get all png files)
 *  - take a set of parameters for the ViBe segmenter from the command line, if no value is given defaults will be used
 *  - will save a set of images showing the motion segmented output
 *  - will optionally (from user) compute performance metrics using a given ground truth image and index
 */

int main (int argc, char * argv[])
{
    // initialise string names, int values, and type double values
    vcl_vector<vcl_string> filenames;
    vcl_string directory, extension, outputDirectory, computeAccuracyAlgorithm, outputFileName;

    int inputRadius, inputBackgroundSamples, inputSubSampling, inputSamples, width, height,
        truePositive = 0, falsePositive = 0, trueNegative = 0, falseNegative = 0, imageCount = 0;

    double tn_rate,fn_rate,tp_rate,fp_rate;

    // This function will take the input parameters stated by the user
    // and return them all to this main
    argumentHandler(argc, argv,  filenames,  directory ,  extension,  outputDirectory, inputRadius, inputBackgroundSamples, inputSubSampling, inputSamples, computeAccuracyAlgorithm, outputFileName);

    // This function will compute the vibe alogirthm segmentation, it will take in
    // all the inputs it needs and then save the files to the output directory. it
    // will also return an image count allowing the next functions to know what the
    // image its stopped at or amount its segmented
    vibeAlgorithm(filenames, outputDirectory, extension, outputFileName, width, height, inputSamples, inputRadius, inputBackgroundSamples, inputSubSampling,imageCount);

    if (computeAccuracyAlgorithm == "yes"){  // if the user wants to compute the metrics then come through this loop. - 'yes' by default

        // This function will compute the algorithm that compares the ground truth
        // to the last image that was segmented through the vibe algorithm. It will
        // compute the true/false positive/negative values as type double in 3 decimal
        // places. It will print these values to a txt file that will be saved as
        // Accuracy_Resuls.txt in the location of the output files as well
        accuracyAlgorithm( outputDirectory, directory, extension, outputFileName, imageCount, truePositive, falsePositive,
                          trueNegative, falseNegative, width, height, tn_rate, fn_rate,
                          tp_rate, fp_rate); //complete calculation and print txt file of output calculations
    }
}


/*
 * Function that will handle the input arguments by the user.
 * It will then return all the arguments back to the user
*/
void argumentHandler(int argc, char * argv[], vcl_vector<vcl_string> &filenames, vcl_string &directory , vcl_string &extension, vcl_string &outputDirectory,int  &inputRadius,  int &inputBackgroundSamples,  int &inputSubSampling, int &inputSamples, vcl_string &computeAccuracyAlgorithm,vcl_string &outputFileName){

    // create arguments for the user to use both strings and interger types
    // input arguments for the algorithm type vlc_string
	vul_arg<vcl_string>
		arg_in_path("-path", "Input path, i.e. C:/somefiles/"),
		arg_in_outputPath("-outputPath","Output path i.e. C:/output/"),
		arg_in_glob("-glob", "Input glob, i.e. *png, this will get all png's."),
        arg_compute_metrics("-computeMetrics","'yes' to compute the accuracy algorithm; 'no' to skip it","yes"),
        arg_output_File_Name("-outputName", "Specify the name you would like your output images to be", "output");

	// input arguments for the algorithm type int
	vul_arg<int> arg_number_sample("-sample", "Sample Amount.", 20),
		arg_number_radius("-radius", " Radius amount.", 20),
        arg_number_backgroundSample("-min", " minimum background samples amount.", 2),
        arg_number_subSampling("-random", " Random sub sampling Amount.", 16);

    // This argparse will go through the command line where the user
    // has specified the values
	vul_arg_parse(argc, argv);


	// if there is no specified directory for output and input, no file extension to look for and they specified compute metrics incorrectly ie not yes or no then
	// it will run this loop and exit the program showing what they need
	if ((arg_in_path() == "") || (arg_in_glob() == "") || (arg_in_outputPath() == "") || ((arg_compute_metrics() != "yes") && (arg_compute_metrics() != "no"))){

		vul_arg_display_usage_and_exit();   //will exit the program and tell the user what they should be using as argument

	}

    // below we will store the command line arguments in variables to pass back to the main so that
    // it can be used
    directory = arg_in_path();
    extension = arg_in_glob();
    outputDirectory = arg_in_outputPath();
    inputRadius = arg_number_radius();
    inputBackgroundSamples = arg_number_backgroundSample();
    inputSubSampling = arg_number_subSampling();
    inputSamples = arg_number_sample();
    computeAccuracyAlgorithm = arg_compute_metrics();
    outputFileName = arg_output_File_Name();

    // loop through a directory using a vul_file_iterator, this will create a list of all files that match "directory + "/*" + extension", i.e. all files in the directory
	// that have our target extension
	// populate the filename vector so that we can call them in the vibe algorithm later
	for (vul_file_iterator fn=(directory + "/*" + extension); fn; ++fn)
	{
		// we can check to make sure that what we are looking at is a file and not a directory
		if (!vul_file::is_directory(fn())){
            // if it is a file, add it to our list of files
			filenames.push_back (fn());
		}
	}

}

/*
 * Function that will handle the vibe algorithm segmentation of each image specified by the user
 * It will return an image counter which represents the amount of images it has segmented
 * before it starts the algorithm it will apply a blur on the image allowing for a better output
*/
void vibeAlgorithm(vcl_vector<vcl_string>filenames, vcl_string outputDirectory, vcl_string extension, vcl_string outputFileName, int &width,int &height,int inputSamples,int inputRadius, int inputBackgroundSamples, int inputSubSampling,int &imageCount){

    ViBe_Model *vibe;   //intialise as a pointer so we can destroy the memory allocation at the end by delete vibe;

    vibe = new ViBe_Model;

    for (unsigned int i = 0; i < filenames.size(); i++){    // iterate through all images

		vil_image_view<unsigned char> newImage = vil_load(filenames[i].c_str());    // grab image to segment

        width = newImage.ni();  // get width of image
        height = newImage.nj(); // get height of image

		vil_image_view<unsigned char> output(width, height, 1);  // create output signle plane image


        vil_gauss_filter_2d(newImage, newImage,2.8,2.8*3,vil_convolve_zero_extend); // blur the image to smooth the pixels, helps with the VIBE segmentation of the image. Better results.

        if (i == 0){    // on the first image we will pass in the images size and all the input parameters that the user specifies

             vibe->Init(inputSamples, inputRadius, inputBackgroundSamples,inputSubSampling,width,height);  // pass intialising parameters

        }

        vibe->Segment(newImage,output); // call member fuction segment() from the vibe class, pass intput image and output image through which wll return the output image as well

        saveOutputImage(outputDirectory ,extension,  outputFileName, (i + 1), output);  //call the saveOutputimage function

        imageCount++;   //increment counter to show how many images we have done
	}

	 delete vibe;    //call deconstructor for vibe model this will deallocate memory of the pixels

}

/*
 * Function that will handle the accuracy algorithm.
 * It compares the last image taken from the vibe algoirthm to the ground truth
 * of the the image
 * It will then save a txt file of the true and false values at 3 decimal places
*/
void accuracyAlgorithm(vcl_string outputDirectory, vcl_string Directory, vcl_string extension, vcl_string outputFileName, int imageCount, int truePositive, int falsePositive,  int trueNegative, int falseNegative, int width, int height, double &tn_rate,double &fn_rate,double &tp_rate , double &fp_rate){

    vcl_stringstream  outputName, tn_rateString, fn_rateString, tp_rateString, fp_rateString, outputTxt;   // intitilise a string streams
    outputName << outputDirectory << outputFileName << imageCount << extension.substr(1);  //name of last image
    vil_image_view<unsigned char> finalSegment = vil_load(outputName.str().c_str());    // grab image to segment
    vil_image_view<unsigned char> GroundTruth = grabGroundTruth(Directory,outputDirectory,extension); //find the ground truth in the original directory and then save it to the new directory

    for (int x = 0; x < width; x++){
        for (int y = 0; y < height; y++){

            // True negative, when both the ground truth and the output image are 0
            if ((GroundTruth(x, y) == 0) && (finalSegment(x, y) == 0)){trueNegative++;}
            // False negative, when the ground truth is non-zero (indicates motion), but the output image is 0
            else if ((GroundTruth(x, y) != 0)  && (finalSegment(x, y) == 0)){falseNegative++;}
            // True positive, when both the ground truth and output image indicate motion
            else if ((GroundTruth(x, y) != 0)  && (finalSegment(x, y) != 0)){truePositive++;}
            // False positive, the ground truth indicates no motion, but the output image shows motion
            else if ((GroundTruth(x, y) == 0)  && (finalSegment(x, y) != 0)){falsePositive++;}

        }
    }

    tn_rate = double(trueNegative) / (double(trueNegative) + double(falsePositive));    //calculate the tn rate
    fn_rate = double(falseNegative) / (double(truePositive) + double(falseNegative));   //calculate the fn rate
    tp_rate = double(truePositive) / (double(truePositive) + double(falseNegative));    //calculate the tp rate
    fp_rate = double(falsePositive) / (double(trueNegative) + double(falsePositive));   //calculate the fp rate

    tn_rateString << vcl_setprecision(3) << tn_rate <<vcl_endl; // create string with 3 decimal place
    fn_rateString << vcl_setprecision(3) << fn_rate <<vcl_endl; // create string with 3 decimal place
    tp_rateString << vcl_setprecision(3) << tp_rate <<vcl_endl; // create string with 3 decimal place
    fp_rateString << vcl_setprecision(3) << fp_rate <<vcl_endl; // create string with 3 decimal place

    outputTxt << outputDirectory << "Accuracy_Results.txt";         //name the txt file and add it ot he directory you want
    ofstream txtFileOutput(outputTxt.str().c_str());                // output location of the txt file
    txtFileOutput   << "tn rate: " << tn_rateString.str() << "\n"   // print the tn rate to txt file
                    << "fn rate: " << fn_rateString.str() << "\n"   // print the fn rate to txt file
                    << "tp rate:  " << tp_rateString.str() << "\n"  // print the tp rate to txt file
                    << "fp rate:  " << fp_rateString.str() << "\n"; // print the fp rate to txt file
}

/*
 * Function that will save output images
 * It wills ave the image to the specified output directory with the extension supplied
 * It will be named 'output' followed by an integer specified by the user or program
*/
void saveOutputImage(vcl_string outputDirectory , vcl_string extension, vcl_string outputFileName, int outputNumber, vil_image_view<unsigned char> imageToSave){

        vcl_stringstream outputName;
        outputName << outputDirectory << outputFileName << outputNumber << extension.substr(1);   // new output name include directory location
        vil_save(imageToSave,outputName.str().c_str() );  //save new image as output[i+1].jpeg
        outputName.str(""); //clear output string so that the names don't keep compiling together
        outputName.clear();

}


/*
 * Function that finds the groundtruth from the specified directory and outputs it to the
 * output directory as the specified extension.
 * It will also return the new groudn truth as an image to be used
*/
vil_image_view<unsigned char>  grabGroundTruth( vcl_string directory, vcl_string outputDirectory, vcl_string extension){

    vcl_stringstream  outputGround;

    outputGround << directory << "groundtruth.bmp"; //grab ground truth location

    vil_image_view<unsigned char> GroundTruth = vil_load(outputGround.str().c_str());    // load the ground truth

    outputGround.str("");   //clear the buffer for ground truth location

    outputGround.clear();   //clear the buffer for ground truth location

    outputGround << outputDirectory << "groundtruth" << extension.substr(1);   // grab the location the user wants to save the new ground truth

    vil_save(GroundTruth,outputGround.str().c_str());   //save the groundtruth to the directories of the segmented images and save as a jpeg

    return GroundTruth;
}


