#include <vil/vil_image_view.h>
#include <vbl/vbl_array_2d.h>
#include <vnl/vnl_random.h>
#include <stdlib.h>
#include "ViBe_Model.h"


#define background 0
#define foreground 255

ViBe_Model::ViBe_Model()
{
    numUpdates = 0; //a counter that helps with identifying first frame
    randomNumberGenerator = new vnl_random();   // construct a random variable to use with in this class
}

ViBe_Model::~ViBe_Model()       // call when exiting program
{
    // de-allocating model - free the memory
    for (int i = 0; i <width; i++){
      for (int j = 0; j < height; j++)

        free(model[i][j]);  // free's j array
        free(model[i]);     // free's i array
    }
    free(model);            // free's sample array
}

void ViBe_Model::Init(int Samples, int Radius, int MinSamplesBackground, int RandomSubsampling, int Width, int Height)
{
	samples = Samples;                              // number of samples per pixel
	radius = Radius;                                // target distance when matching pixel
	minSamplesBackground = MinSamplesBackground;    // number of samples to match to be considered background
	randomSubsampling = RandomSubsampling;          // how often to update the samples
	width = Width;                                  // model width
	height = Height;                                // model height

    CreateModel();  //call local member function which allocates memory for all the pixels - Samples * Width * Height  =  memory size
}

void ViBe_Model::Segment(vil_image_view<unsigned char>& input, vil_image_view<unsigned char>& output)
{

    if(numUpdates == 0){    //initialise ViBePixel on the first frame
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                for(int k = 0; k < samples; k++){

                    model[i][j][k].backgroundUpdate(input(i,j,0),input(i,j,1),input(i,j,2));    // inserting values of current image (first frame only) in the array of Pixels
                }
            }
        }

        numUpdates++;   // changing this allows for the program to not access this loop again

    }

    for ( int x = 0; x < width; x++){           //max width
        for ( int y = 0; y < height; y++){      //max height

            int count = 0, index = 0;         //initialise variables

            while((count < minSamplesBackground) && (index < samples)){     // check how many samples are within the radius of our input pixel

                model[x][y][index].backgroundCompare( input(x,y,0), input(x,y,1), input(x,y,2), radius, count, index);  // apply euclidean distances and return count and index

            }

            if(count >= minSamplesBackground){  //if true it is a background pixel

                output(x,y) = background;    //set the output to background ie. black

                int random = randomNumberGenerator->lrand32(0, randomSubsampling - 1);  // create a random value between 1 and SubSampling - 1

                if(random == 0){

                    random = randomNumberGenerator->lrand32(0, samples - 1);    // new random value between 0 and samples - 1

                    model[x][y][random].backgroundUpdate(input(x,y,0),input(x,y,1),input(x,y,2));   //update the background pixels

                }

                random = randomNumberGenerator->lrand32(0, randomSubsampling - 1 - 1);  // create a random value between 1 and SubSampling - 1 - 1

                if(random == 0){

                    int xrand, yrand;

                    PickNeighbour( x, y, xrand,  yrand);    // find a random neighbour and return xrand and yran

                    random = randomNumberGenerator->lrand32(0, samples - 1);    // create a random value between 1 and sample - 1

                    model[xrand][yrand][random].backgroundUpdate(input(x,y,0),input(x,y,1),input(x,y,2));   //update the neighbouring pixel

                }
            }
            else{

                output(x,y) = foreground;   // output pixel is foreground ie white. meaning it is an object

            }
        }
    }

}

void ViBe_Model::PickNeighbour(int x, int y, int& nX, int& nY)
{
   /*   The below is a representation of the neighbouring values we want
    *
    *               [x-1 , y-1]   [x , y-1]  [x+1 , y-1]
    *               [x-1 ,  y ]   [x ,  y ]  [x+1 ,  y ]
    *               [x-1 , y+1]   [x , y+1]  [x+1 , y+1]
    *
    */

    int randomValueX = randomNumberGenerator->lrand32(-1, 1);   //grab random value from -1 to 1
    int randomValueY = randomNumberGenerator->lrand32(-1, 1);   //grab random value from -1 to 1

    nX = (randomValueX) + x;   //randomise a new X value that is surrounding the original x
    nY = (randomValueY) + y;   //randomise a new Y value that is surrounding the original y

    // check image boundary conditions
    if (nX < 0){    //is the new value out of bounds to the left of the image

        randomValueX = randomNumberGenerator->lrand32(0, 1);    // grab a new random value this time between 0 : 1 so it stays within the boundary
        nX = randomValueX + x;      // recalculate new neigbour

    }
    else if (nX > width){

        randomValueX = randomNumberGenerator->lrand32(-1, 0);   // grab a new random value this time between -1 : 0 so it stays within the boundary
        nX = x + (randomValueX);    // recalculate new neigbour

    }
    if (nY < 0){

        randomValueY = randomNumberGenerator->lrand32(0, 1);    // grab a new random value this time between 0 : 1 so it stays within the boundary
        nY = randomValueY + y;      // recalculate new neigbour

    }
    else if (nY > height){

        randomValueY = randomNumberGenerator->lrand32(-1, 0);   // grab a new random value this time between -1 : 0 so it stays within the boundary
        nY = y + randomValueY;      // recalculate new neigbour

    }
}

void ViBe_Model::CreateModel()
{
    /*   The below is a representation of the 3d array:  Sample Size = 1 Row Size = 3 Column Size = 2
    *    Model***      Model**   Model*
    *
    *                           Column[0]
    *                          /
    *                    Row[0]
    *                   /      \
    *                  /        Column[1]
    *                 /
    *                /          Column[0]
    *               /          /
    *   Sample[1]--- ----Row[1]
    *               \          \
    *                \          Column[1]
    *                 \
    *                  \        Column[0]
    *                   \      /
    *                    Row[2]
    *                          \
    *                           Column[1]
    *
    *
    */
    model = new ViBe_Pixel**[width+1];  // Allocate memory for the amount of rows

    for ( int i = 0; i <= width; i++){

        model[i]  = new ViBe_Pixel*[height+1]; // Allocate memory to point to for the columns.

        for ( int j = 0; j <= height; j++){

            model[i][j]  = new ViBe_Pixel[samples+1]; // Allocate memory to point to for the samples.

        }
    }
}
